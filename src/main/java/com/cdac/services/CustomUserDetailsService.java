package com.cdac.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cdac.mappers.CustomerMapper;
import com.cdac.pojo.Customer;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		String customQuery = "SELECT * FROM customer WHERE email = '" + username + "'";

		Customer customer = jdbcTemplate.queryForObject(customQuery, new CustomerMapper());

		return new User(customer.getEmail(), customer.getPassword(), getAuthorities(customer));

	}

	public Collection<? extends GrantedAuthority> getAuthorities(Customer customer) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(customer.getRole()));
		return authorities;
	}

}
