package com.cdac.pojo;

public class Customer {

	private String email;
	private String password;
	private Boolean enabled;
	private String role;

	public Customer() {

	}

	public Customer(String email, String password, Boolean enabled, String role) {
		super();
		this.email = email;
		this.password = password;
		this.enabled = enabled;
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Customer [email=" + email + ", password=" + password + ", enabled=" + enabled + ", role=" + role + "]";
	}

}
